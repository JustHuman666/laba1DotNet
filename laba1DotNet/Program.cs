﻿using System;
using System.Collections.Generic;
using StackLibrary;

namespace laba1DotNet
{
    class Program
    {
        static void Main(string[] args)
        {
            IdealStack<int> MyStack = new IdealStack<int>(new List<int> {1, 4, 6, 3, 5});
            Console.WriteLine("Stack is created.");
            MyStack.Pushed += (stack, eventResult) => Console.WriteLine(eventResult.message + " " + eventResult.value);
            MyStack.Popped += (stack, eventResult) => Console.WriteLine(eventResult.message + " " + eventResult.value);
            MyStack.Cleared += (stack, eventResult) => Console.WriteLine(eventResult.message);
            MyStack.ConvertedToArray += (stack, eventResult) => DisplayMessageWithArray(eventResult.message, eventResult.array);
            MyStack.Coppied += (stack, eventResult) => DisplayMessageWithArray(eventResult.message, eventResult.array);

            int[] Array1 = MyStack.ToArray();
           
            MyStack.Push(9);

            int FirstElement = MyStack.Peek();
            Console.WriteLine($"First element in the stack is: {FirstElement}");

            int ElementToDelete = MyStack.Pop();

            int[] Array = new int[10];
            MyStack.CopyTo(Array, 1);

            Console.ReadKey();
        }
        private static void DisplayMessageWithArray(string message, string [] array)
        {
            Console.WriteLine(message);
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }

    }
}
